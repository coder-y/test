const a = [], b = [], o = {};

let c = 0;

for (let i = 0; i < 1000000; i++) {
    a.push(Math.random().toFixed(6));
    b.push(Math.random().toFixed(6));
}

const d1 = new Date().getTime();
for (let it of a) {
    o[it] = '';
}

for (let it of b) {
    if (o[it] !== undefined) c++;
}
const d2 = new Date().getTime() - d1;
console.log(d2, 'ms');
console.log(c);
