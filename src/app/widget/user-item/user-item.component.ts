import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {
  @Input() online:boolean;
  @Input() cnt:number;
  @Input() cam:boolean;
  @Input() route:string;

  constructor() { }

  ngOnInit() {
  }

}
